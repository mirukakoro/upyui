print('uPyUI Wifi Config')

exit_ = 0

import network

nic  = network.WLAN()
nic.active(1)

def scan():
    print('SSID BSSID RSSI Authmode Hidden')
    for wifi in nic.scan():
        for i in wifi:
            print(str(i), end=' ')
        print()
    print()

def set_ifconfig():
    print('Set ifconfig')
    print()
    print('Current config is:')
    config = nic.ifconfig()
    print('IP Address:  {}'.format(config[0]))
    print('Subnet Mask: {}'.format(config[1]))
    print('Gateway:     {}'.format(config[2]))
    print('DNS Server:  {}'.format(config[3]))
    choice = input('Keep[Y/n]? ').lower()
    if choice == 'y':
        print('Keeping current config.')
    elif choice == 'n':
        ip      = input('IP Address:  ')
        subnet  = input('Subnet Mask: ')
        gateway = input('Gateway:     ')
        dns     = input('DNS Server:  ')
        nic.ifconfig((ip, subnet, gateway, dns))
    else:
        print('Unknown. Keeping current config.')

def connect():
    nic.connect(input('SSID: '), input('Pass: '))

def skip():
    pass

status = {network.STAT_IDLE:'Not Connected', network.STAT_CONNECTING:'Connecting...', network.STAT_WRONG_PASSWORD:'Bad Password ', network.STAT_NO_AP_FOUND:'No AP Found  ', network.STAT_CONNECT_FAIL:'Unknown Error', network.STAT_GOT_IP:'Got IP       '}

cmds = {'scan':scan, 'ifconfig':set_ifconfig, 'ipconfig':set_ifconfig, 'connect':connect, '':skip}

print('Commands: scan, ifconfig/ipconfig, connect, exit')
while 1:
    print(status[nic.status()])
    inp = input('Run ')
    if inp == 'exit':
        break
    try:
        cmds[inp]()
    except KeyError:
        print('Command not found')